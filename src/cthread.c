#include "fila2.h"
#include <ucontext.h>
#include "cdata.h"
#include "cthread.h"
#include <stdio.h>
#include <stdlib.h>


FILA2 blocked;
FILA2 ready;
FILA2 finished;
enum state {CREATED, READY, EXEC, BLOCKED, ENDED};
TCB_t *running;
TCB_t main_tcb;

int tids = 1;
int first_call = 0; 

// contexto do escalonador
ucontext_t scheduler_context;
char scheduler_stack[SIGSTKSZ];
ucontext_t terminate_context;
char terminate_stack[SIGSTKSZ];

// funções do escalonador
int scheduler();
int initialize();
void terminate();
int dispatch(TCB_t *task); 
int unblock_thread(int tid);

int ccreate (void* (*start)(void*), void *arg) {
	char *stack = malloc(sizeof(char) * SIGSTKSZ);

	// se for a primeira thread criada, é preciso setar a main_tcb como o fluxo principal do programa
	if(first_call == 0) {
		initialize();

		//como o contexto atual é o da main thread, cria contexto atual pra main thread.
		getcontext(&main_tcb.context);
		ucontext_t *context = &main_tcb.context;
		context->uc_link = &terminate_context;
		context->uc_stack.ss_sp = stack;
		context->uc_stack.ss_size = sizeof(char) * SIGSTKSZ;
		makecontext(context, (void (*)(void)) start, 1, arg);
	}

	// cria nova TCB
	TCB_t *tcb = malloc(sizeof(TCB_t));
	tcb->tid = tids++;
	tcb->state = READY;
	
	// cria novo contexto para tcb->context a partir do atual
	getcontext(&tcb->context);
	ucontext_t *context = &tcb->context;
	context->uc_link = &terminate_context;
	context->uc_stack.ss_sp = stack;
	context->uc_stack.ss_size = sizeof(char) * SIGSTKSZ;
	makecontext(context, (void (*)(void)) start, 1, arg);

	// adicionar TCB na fila de aptos
	AppendFila2(&ready,(void *)tcb);

	return tcb->tid;
}

int cyield(void) {
	if(first_call == 0)
		initialize();

	if(!running)
		return -1;

	running->state = READY;
	AppendFila2(&ready,(void *)running);
	swapcontext(&running->context, &scheduler_context);

	return 0;
}

int cjoin(int tid) {
	FirstFila2(&blocked);

	// casos que retornam erro:
	// 1. thread a esperar já terminou
	FirstFila2(&finished);
	if(finished.it) {
		TCB_t *tcb = (TCB_t*)GetAtIteratorFila2(&finished);
		if(tcb->tid == tid)
			return -1; // thread está na lista de terminados
		
		while(NextFila2(&finished) == 0) {
			tcb = (TCB_t*)GetAtIteratorFila2(&finished);
			if(tcb) {
				if(tcb->tid == tid) 
					return -1; // thread está na lista de terminados
			}
		}
	}

	// 2. thread com este tid ainda não foi criada
	if(tid > tids)
		return -1;
	
	// 3. alguma thread da lista de blocked já está esperando por esta thread
	if(blocked.it) {
		FirstFila2(&blocked);
		wTCB_t *wtcb = (wTCB_t *)GetAtIteratorFila2(&blocked);
		if(wtcb->waited_tid == tid)
			return -1; // alguma thread estava esperando por tid
	
		while(NextFila2(&blocked) == 0) {
			wtcb = (wTCB_t *)GetAtIteratorFila2(&blocked);
			if(wtcb) {
				if(wtcb->waited_tid == tid) 
					return -1; //// alguma thread estava esperando por tid
			}
		}
	}

	// se chegou até aqui, não encontrou nenhum problema
	wTCB_t *w = malloc(sizeof(wTCB_t));
	w->tcb = running;
	w->waited_tid = tid;
	w->tcb->state = BLOCKED;
	AppendFila2(&blocked,(void *)w);

	swapcontext(&running->context, &scheduler_context);
	
	return 0; // sucesso!
}

int csem_init(csem_t *sem, int count){
	sem->count = count;		
	sem->fila = malloc(sizeof(struct sFila2));	
	CreateFila2(sem->fila);

	if(sem && sem->fila)
		return 0;
	else
		return -1;
}

/* solicitar recurso
recurso livre -> ele é atribuido a thread e tudo continua igual
recurso nlivre -> thread colocada na lista de bloqueados e posta a espera desse recurso na fila
 */
int cwait(csem_t *sem){
	if(!sem)
		return -1;

	sem->count--;

	//recurso ocupado
	if (sem->count < 0) {
		running->state = BLOCKED;
		AppendFila2(sem->fila,(void *)running);
		swapcontext(&running->context, &scheduler_context);

		return 0;
	}

	return 0;	
}

//increments the value of semaphore by 1. After the increment, if the pre-increment value was negative (meaning there are processes waiting for a resource), it transfers a blocked process from the semaphore's waiting queue to the ready queue
int csignal(csem_t *sem) {
	if(!sem)
		return -1;

	sem->count++;
	if(sem->count <= 0) { // há processos esperando na lista do semáforo.
		FirstFila2(sem->fila);

		if(sem->fila->it) {
			TCB_t *task = (TCB_t*)GetAtIteratorFila2(sem->fila);
			if(task) {
				task->state = READY;
				AppendFila2(&ready,(void *)task);
				DeleteAtIteratorFila2(sem->fila);

				return 0;
			} else
				return -1;
		}
	}
	return 0;
}

int scheduler() {
	FirstFila2(&ready);
	TCB_t *task = (TCB_t *)GetAtIteratorFila2(&ready);

	if(task) {
 		DeleteAtIteratorFila2(&ready);
		dispatch(task);
	}

	printf("There are no more threads to schedule. Exiting...\n");

	exit(0);
}

int initialize() {
	first_call = 1;

	getcontext(&scheduler_context);
	scheduler_context.uc_link = NULL; 
	scheduler_context.uc_stack.ss_sp = scheduler_stack;
	scheduler_context.uc_stack.ss_size = sizeof(scheduler_stack);
	makecontext(&scheduler_context, (void (*)(void))scheduler, 0);

	getcontext(&terminate_context);
	terminate_context.uc_link = NULL; 
	terminate_context.uc_stack.ss_sp = terminate_stack;
	terminate_context.uc_stack.ss_size = sizeof(terminate_stack);
	makecontext(&terminate_context, (void (*)(void))terminate , 0);

	CreateFila2(&ready);
	CreateFila2(&blocked);
	CreateFila2(&finished);
	running = malloc(sizeof(TCB_t));

	main_tcb.tid = 0;
	main_tcb.state = READY;

	running = &main_tcb;

	return 0;
}

void terminate() {
	running->state = ENDED;
	free(running->context.uc_stack.ss_sp);
	AppendFila2(&finished,(void *)running);
	unblock_thread(running->tid);
	setcontext(&scheduler_context);
}

int dispatch(TCB_t *task) {
	task->state = EXEC;
	running = task;
	setcontext(&task->context);

	return -1; //algo deu errado
}

int unblock_thread(int tid) {
	/* para desbloquear a thread joinada, tem que percorrer 
	 * a lista de bloqueados e verificar se há alguma tcb com 
	 * waited_tid = tid. 
	 */
	
	FirstFila2(&blocked);
	//não há processos bloqueados - logo, não há ninguém esperando por tid
	if(!blocked.it)	
		return -1;
	
	//waited_pid do primeiro da fila de bloqueados é = pid:
	wTCB_t *wtcb = (wTCB_t*)GetAtIteratorFila2(&blocked);
	if(wtcb) {
		if(wtcb->waited_tid == tid) {
			wtcb->tcb->state = READY;
			AppendFila2(&ready,(void *)wtcb->tcb);
			DeleteAtIteratorFila2(&blocked);
			//setcontext(&wtcb->tcb->context);
			return 0;
		}
	}	

	//itera sobre o resto da fila checando a mesma coisa.
	while(NextFila2(&blocked) == 0) {
		wtcb = (wTCB_t*)GetAtIteratorFila2(&blocked);
		if(wtcb) {
			if(wtcb->waited_tid == tid) {
				wtcb->tcb->state = READY;
				AppendFila2(&ready,(void *)wtcb->tcb);
				DeleteAtIteratorFila2(&blocked);
				//setcontext(&wtcb->tcb->context);
				return 0;
			}
		}
	}

	return -1; //nenhuma thread estava aguardando por tid.
}
