#include<stdlib.h>
#include "../include/fila2.h"
#include "../include/cthread.h"
#include <stdio.h>

void *f0(void *arg){
	printf("Thread 0: %d\n", *((int *) arg));
	return NULL;
}

void *f2(void *arg)
{
	printf("Thread 2: criada pela thread 1.\n");
	return NULL;
}

void *f1(void *arg){
	int tid;
	printf("Thread 1: %d\n", *((int *) arg));
	tid = ccreate(f2, NULL);
	printf("Thread 1 criou nova thread (thread 2)\n");
	return NULL;
}

int main(){
	int val=50, tid;
	
	tid = ccreate(f0, (void *) &val);
	printf("Criou thread 0.\n");
	cyield();

	tid = ccreate(f1, (void *) &val);
	printf("Criou thread 1.\n");
	cyield();

	cyield();

	printf("Retornando à main e encerrando o programa.\n");

	exit(0);
}
