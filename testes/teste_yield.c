#include<stdlib.h>
#include "../include/fila2.h"
#include "../include/cthread.h"
#include <stdio.h>

void *f0(void *arg){
	printf("Thread: 1 \n Imprimindo %d\n", *((int *) arg));
	return NULL;
}

void *f1(void *arg){
	printf("Thread: 2 \n Imprimindo %d\n", *((int *) arg));
	cyield();
	printf("Thread: 2 \n imprimindo %d\n", *((int *) arg));
	return NULL;
}

void *f2(void *arg){
	printf("Thread: 3 \n Imprimindo %d\n", *((int *) arg));
	cyield();
	printf("Thread: 3 \n Imprimindo %d\n", *((int *) arg));
	cyield();
	printf("Thread: 3 \n Imprimindo %d\n", *((int *) arg));
	return NULL;
}

int main(){
	int var=0;

	ccreate(f0, (void *) &var);
	var++;
	printf("Main após a criacao da thread 0, var = %d\n", var);
	cyield();
	ccreate(f1, (void *) &var);
	var++;
	cyield();
	ccreate(f2, (void *) &var);
	var++;
	cyield();
	var++;
	cyield();
	var++;
	cyield();
	var++;
	cyield();

	printf("Retornando à main e encerrando...\n");
	
	exit(0);
}
