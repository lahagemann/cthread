#include<stdlib.h>
#include<unistd.h>
#include<pthread.h>
#include<errno.h>

#include "../include/fila2.h"
#include "../include/cthread.h"
#include <stdio.h>

#define THREADS 3

csem_t sem;
int tid[THREADS];
csem_t mut;
int wait;

void* semaphore(void *arg){
	int index = (int)(intptr_t)arg;

	printf("Thread %d executando.\n", tid[index]);
	cwait(&sem);
	printf("Thread %d entra na seção critica.\n", tid[index]);
	printf("\n");
	printf("Thread %d yielded\n", tid[index]);
	cyield();
	printf("Thread %d volta a seção critica.\n ", tid[index]);
	printf("\n");
	printf("Thread %d sai da seção critica.\n", tid[index]);
	csignal(&sem);
	return NULL;
}

int main(){
	int i = 0; 
	int *index;

	csem_init(&sem, 1);
	
	for(i=0; i<THREADS; i++) {
		index = malloc(sizeof(int));
		index = i;
		tid[i] = ccreate(semaphore, (void *) index);
	}
	
	for(i = 0; i<THREADS; i++) 
		cjoin(tid[i]);
	

	printf("\nMain terminando o programa...\n");
	exit(0);

}
